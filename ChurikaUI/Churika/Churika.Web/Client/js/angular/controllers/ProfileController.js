﻿'use strict';
angular.module('app').controller('ProfileController', ['$scope', '$stateParams', '$location', 'userFactory', 'oidcManager',
    function ($scope, $stateParams, $location, userFactory, oidcManager) {

    $scope.user = {};
    $scope.errors = [];
    $scope.isUpdated = false;
    $scope.dataLoading = false;
    $scope.isSubmitted = false;    
    $scope.userProfile = function() {
        userFactory.getProfile()
            .success(function(data) {
                console.log(data);
            })
             .error(function (response) {
                 $scope.isSubmitted = false;
                 parseErrors(response);
             });
    }    
    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);