﻿'use strict';
angular.module('app').controller('RestaurantController', ['$scope', 'restaurantFactory', 'restaurantDataService', '$stateParams', '$rootScope', '$state', 'restaurantsListObj',
    function ($scope, restaurantFactory, restaurantDataService, $stateParams, $rootScope, $state, restaurantsListObj) {
        $scope.restaurantList = [];


        var restaurantsInfo = restaurantsListObj.result;
        console.log(restaurantsListObj);

        if (restaurantsInfo.TotalCount > 0) {
            $scope.totalCount = restaurantsInfo.TotalCount;
            $scope.page = restaurantsInfo.Page;
            $scope.totalPages = restaurantsInfo.TotalPages;
        }

        if (restaurantsInfo.Count > 0) {
            $scope.showNoRestaurantsFound = false;
            for (var i = 0; i < restaurantsInfo.Count; i++) {
                //restaurantDataService.restaurants.push(data.Items[i]);
                $scope.restaurantList.push(restaurantsInfo.Items[i]);
            }
        } else {
            $scope.showNoRestaurantsFound = true;
        }

        $scope.currentPage = 1;


        $scope.slides = [{ "img": 'http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg', "text": "Samosa" },
        { "img": 'http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg', "text": "Noodles" },
        { "img": 'http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg', "text": "Pizza" },
        { "img": 'http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg', "text": "Italian" }
        ];
        $scope.foodCategory = $stateParams.query;

        function updateQueryParameters() {
            var searchParamaters = restaurantsListObj.searchParams;
            searchParamaters["query"] = $stateParams.query;
            restaurantFactory.saveSearchParams(searchParamaters);
        }
        $scope.getRestaurantByType = function (value) {
            $stateParams.query = value;
            updateQueryParameters();
            $state.go('app.restaurants.list', $stateParams, { reload: true });
        };

        $scope.hoveringOver = function (value) {
            $scope.overStar = value;
            $scope.percent = 100 * (value / $scope.max);
        };
        $scope.showRestaurentDetail = function (restaurant) {
            $rootScope.restaurantDetail = restaurant;
            $state.go("app.restaurants.detail", { id: restaurant.RestaurantId });
        }

        var errorCallBack = function (response) {
            console.log(response);
        };

        function getRestaurants() {
            restaurantFactory.searchRestaurants(stateInfo.searchInfo.lat, stateInfo.searchInfo.lon, stateInfo.searchInfo.query, $scope.currentPage)
        .then(function (response) {
            if (response.data.TotalCount > 0) {
                $scope.totalCount = response.data.TotalCount;
                $scope.page = response.data.Page;
                $scope.totalPages = response.data.TotalPages;
            }

            if (response.data.Count > 0) {
                $scope.showNoRestaurantsFound = false;
                for (var i = 0; i < response.data.Count; i++) {
                    //restaurantDataService.restaurants.push(data.Items[i]);
                    $scope.restaurantList.push(response.data.Items[i]);
                }
            } else {
                $scope.showNoRestaurantsFound = true;
            }
        }).catch(errorCallBack);
        }



        $scope.loadMore = function () {
            $scope.currentPage++;
            getRestaurants();
        }
    }]);