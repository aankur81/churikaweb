﻿'use strict';
angular.module('app').controller('AddressController', ['$scope', '$rootScope', 'deliveryService', '$stateParams', '$location', 'userFactory', 'oidcManager', '$state', 'localStorageService', 'restaurantFactory', function ($scope, $rootScope, deliveryService, $stateParams, $location, userFactory, oidcManager, $state, localStorageService, restaurantFactory) {

    $scope.user = {};
    $scope.errors = [];
    $scope.isUpdated = false;
    $scope.dataLoading = false;
    $scope.states = {
        "options": [
          { "k": "", "v": "" },
          { "k": "AK", "v": "Alaska" },
          { "k": "AL", "v": "Alabama" },
          { "k": "AR", "v": "Arkansas" },
          { "k": "AZ", "v": "Arizona" },
          { "k": "CA", "v": "California" },
          { "k": "CO", "v": "Colorado" },
          { "k": "CT", "v": "Connecticut" },
          { "k": "DC", "v": "Washington D.C." },
          { "k": "DE", "v": "Delaware" },
          { "k": "FL", "v": "Florida" },
          { "k": "GA", "v": "Georgia" },
          { "k": "HI", "v": "Hawaii" },
          { "k": "IA", "v": "Iowa" },
          { "k": "ID", "v": "Idaho" },
          { "k": "IL", "v": "Illinois" },
          { "k": "IN", "v": "Indiana" },
          { "k": "KS", "v": "Kansas" },
          { "k": "KY", "v": "Kentucky" },
          { "k": "LA", "v": "Louisiana" },
          { "k": "MA", "v": "Massachusetts" },
          { "k": "MD", "v": "Maryland" },
          { "k": "ME", "v": "Maine" },
          { "k": "MI", "v": "Michigan" },
          { "k": "MN", "v": "Minnesota" },
          { "k": "MO", "v": "Missouri" },
          { "k": "MS", "v": "Mississippi" },
          { "k": "MT", "v": "Montana" },
          { "k": "NC", "v": "North Carolina" },
          { "k": "ND", "v": "North Dakota" },
          { "k": "NE", "v": "Nebraska" },
          { "k": "NH", "v": "New Hampshire" },
          { "k": "NJ", "v": "New Jersey" },
          { "k": "NM", "v": "New Mexico" },
          { "k": "NV", "v": "Nevada" },
          { "k": "NY", "v": "New York" },
          { "k": "OH", "v": "Ohio" },
          { "k": "OK", "v": "Oklahoma" },
          { "k": "OR", "v": "Oregon" },
          { "k": "PA", "v": "Pennsylvania" },
          { "k": "RI", "v": "Rhode Island" },
          { "k": "SC", "v": "South Carolina" },
          { "k": "SD", "v": "South Dakota" },
          { "k": "TN", "v": "Tennessee" },
          { "k": "TX", "v": "Texas" },
          { "k": "UT", "v": "Utah" },
          { "k": "VA", "v": "Virginia" },
          { "k": "VT", "v": "Vermont" },
          { "k": "WA", "v": "Washington" },
          { "k": "WI", "v": "Wisconsin" },
          { "k": "WV", "v": "West Virginia" },
          { "k": "WY", "v": "Wyoming" }
        ]
    }
    $scope.contact = {
        "Id": 0,
        "UserId": 0,
        "AddressType": 0,
        "Address1": '',
        "Address2": '',
        "Address3": '',
        "City": '',
        "StateCode": '',
        "PhoneNumber": '',
        "PostalCode": '',
        "IsPreferred": '',
        "UpdatedDate": ''
    }
    var userAddress = localStorageService.get("deliveryAddress");
    if (userAddress) {
        var deliveryAddress = angular.copy(userAddress);
        $scope.contact.Address1 = deliveryAddress.Address1;
        $scope.contact.Address2 = deliveryAddress.Address2;
        $scope.contact.City = deliveryAddress.City;
        $scope.contact.StateCode = deliveryAddress.StateCode;
        $scope.contact.PostalCode = deliveryAddress.PostalCode;
    }
    //function getAddressList() {
    //    deliveryService.getDeliveryAddress()
    //            .success(function (response) {
    //                debugger;
    //                $scope.deliveryAddress = [];
    //                $scope.deliveryAddress = response;
    //            })
    //            .error(function (response) {
    //                console.log(response);
    //            });
    //}
    //getAddressList();

    $scope.submitAddress = function (valid, type) {        
        if (valid) {
            $scope.contact.UpdatedDate = new Date();
            if (type == 'add') {
                deliveryService.addDeliveryAddress($scope.contact)
                    .success(function (response) {
                        deliveryService.postFilledAddress($state.params.cartId);
                        $state.go('app.checkout.pay', { cartId: $state.params.cartId });
                    })
                    .error(function (response) {
                        console.log(response);
                    });
            }
            else if (type == 'update') {
                deliveryService.updateDeliveryAddress(address)
                    .success(function (response) {
                        $state.go('app.checkout.pay', { cartId: $state.params.cartId });
                    })
                    .error(function (response) {
                        console.log(response);
                    });
            }
        }
    }

    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);