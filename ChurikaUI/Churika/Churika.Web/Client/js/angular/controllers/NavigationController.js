﻿'use strict';
angular.module('app').controller('NavigationController', [
    '$scope', 'oidcManager', '$location', 'cartService', '$state', '$rootScope', function ($scope, oidcManager, $location, cartService, $state, $rootScope) {

        var mgr = oidcManager.OidcTokenManager;

        $scope.profileName = '';
        if (mgr.expired) {
            $rootScope.isSignedIn = false;
        } else {

            $rootScope.isSignedIn = true;
            $scope.profileName = mgr.profile.given_name;
            $scope.picture = mgr.profile.picture;
        }
        $scope.logout = function () {
            sessionStorage.removeItem('appState');
            mgr.redirectForLogout();
        }

        $scope.login = function () {
            mgr.redirectForToken();
        }

        $scope.$on('itemAdded', function () {
            $scope.cartItemsCount = $scope.cartItemsCount + 1;
            console.log($scope.cartItemsCount);
        });

        $scope.$on('itemRemoved', function () {
            $scope.cartItemsCount = $scope.cartItemsCount - 1;
        });

        //var cartId = cartService.getId();
        //if (cartId) {
        //    cartService.getItems(cartId)
        //        .success(function (data) {
        //            if (data && data.CartItems) {
        //                cartService.Items = data.CartItems;
        //                var count = data.CartItems.length;
        //                $scope.cartItemsCount = count;
        //            }
        //        })
        //        .error(function (response, status) {
        //            console.log(status);
        //        });
        //} else {
        //    $scope.cartItemsCount = 0;
        //}

        $scope.cartClick = function () {
            //$state.go('app.cart', { cartId: cartId });
            $scope.showTabDialog();
        }
    }]);