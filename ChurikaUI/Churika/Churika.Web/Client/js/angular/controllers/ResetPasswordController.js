﻿'use strict';
angular.module('app').controller('ResetPasswordController', ['$scope', '$stateParams', '$location', 'userFactory', 'oidcManager', function ($scope, $stateParams, $location, userFactory, oidcManager) {

    $scope.user = {};
    $scope.errors = [];
    $scope.isUpdated = false;
    $scope.isSubmitted = false;
    $scope.code = $stateParams.code;
    console.log($scope.code);

    $scope.resetPwd = function () {
        $scope.resetModel = { code: $scope.code, newPassword: $scope.user.newPassword, email: $scope.user.email }
        userFactory.resetPassword($scope.resetModel)
            .success(function (data) {
                $scope.response = "Password Update Successful!";
                $scope.isUpdated = true;
            })
            .error(function (message) {
                $scope.errors = parseErrors(message);
                $scope.isUpdated = false;
            });
    }

    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);