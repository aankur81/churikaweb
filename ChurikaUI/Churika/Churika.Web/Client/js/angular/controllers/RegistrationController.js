﻿'use strict';
angular.module('app').controller('RegistrationController', ['$scope', '$stateParams', '$location', 'userFactory', 'oidcManager', function ($scope, $stateParams, $location, userFactory, oidcManager) {

    $scope.newUser = {};
    $scope.errors = [];

    $scope.register = function () {
        userFactory.submitRegistration($scope.newUser)
            .success(function (response) {
                oidcManager.OidcTokenManager.redirectForToken();
            })
            .error(function (message) {
                if (!message) {
                    $scope.errors.push("We are unable to process your request at this point. Please try again!");
                    return;
                }
                $scope.errors = parseErrors(message);;
            });
    }

    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);