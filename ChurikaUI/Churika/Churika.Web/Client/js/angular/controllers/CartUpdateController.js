﻿'use strict';

angular.module('app')
    .controller('CartUpdateController',
    [
        '$scope', 'restaurantFactory', 'cartService', 'itemInfo','$timeout','$mdDialog', 'localStorageService', 'alertService',
        function ($scope, restaurantFactory, cartService, itemInfo, $timeout,$mdDialog, localStorageService, alertService) {
            $scope.item = itemInfo;
            var cartId = cartService.getId();
            var price = angular.copy($scope.item.Price);
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.changeQuantity = function (item) {
                item.Price = price * item.Quantity;
            };

            $scope.updateItem = function (isValid, cartItem) {
                if (cartId) {
                    if (isValid) {
                        var cartEditedItem = {
                            "Id": $scope.item.Id,
                            "MenuId": $scope.item.MenuId,
                            "Quantity": $scope.item.Quantity,
                            "Notes": $scope.item.Notes == null ? "" : $scope.item.Notes,
                            "RestaurantId": parseInt($scope.item.restaurantId)
                        }
                        cartService.updateItem(cartEditedItem, cartId)
                            .success(function (data) {
                                if (data) {
                                    $mdDialog.cancel();
                                    $timeout(function () {
                                        angular.element("#cart_btn").trigger('click');
                                    }, 200);
                                    //alertService
                                    //    .showAlertMessage('Success', "You cart updated successfully.", "success");
                                }
                            })
                            .error(function (msg) {
                                console.log(msg);
                            });
                    }
                }
            }
        }
    ]);
