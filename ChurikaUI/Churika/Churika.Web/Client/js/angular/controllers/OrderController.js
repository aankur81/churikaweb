﻿'use strict';
angular.module('app').controller('OrderController', ['$scope', '$stateParams', 'orderFactory', 'oidcManager', '$location', function ($scope, $stateParams, orderFactory, oidcManager, $location) {

    var mgr = oidcManager.OidcTokenManager;
    debugger;
    var orderId = $stateParams.id;
    function getOrderInformation() {
        orderFactory.getOrder(orderId)
        .success(function (data) {
            console.log(data);
            $scope.orderDetail = data;
        }).error(function (response) {
            console.log(response);
        });
    }

    if (mgr.expired) {
        localStorage["appState"] = $location.url();
        mgr.redirectForToken();

    }
    else {
        getOrderInformation();
    }

}]);