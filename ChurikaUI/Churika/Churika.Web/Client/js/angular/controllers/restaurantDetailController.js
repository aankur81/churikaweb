﻿'use strict';
angular.module('app').controller('RestaurantDetailController', [
    '$scope', 'restaurantFactory', 'restaurantDataService', '$stateParams', '$uibModal', '$state', 'cartService', '$rootScope', '$mdDialog', 'localStorageService', 'alertService',
    function ($scope, restaurantFactory, restaurantDataService, $stateParams, $uibModal, $state, cartService, $rootScope, $mdDialog, localStorageService, alertService) {
        $scope.restaurantId = $stateParams.id;
        $scope.restAddress = {};
        $scope.menuItems = {};
        $scope.animationsEnabled = true;
        $scope.itemInfo = {};

        $scope.users = [
    {
        name: { first: 'try', last: 'try' }
    },
    {
        name: { first: 'try2', last: 'try2' }
    }];

        $scope.open = function (ev, restItem) {
            var id = cartService.getId();
            if (id)
                cartService.getItems(id)
                    .success(function (data) {
                        if (data.Restaurant.Id !== parseInt($scope.restaurantId, 0) && id && data.CartItems.length > 0) {
                            alertService.showAlertMessage('Error',
                                "You are trying to add menu from another restaurant to this cart",
                                "error",
                                true);
                        } else {
                            openSelectionModal();
                        }
                    }).error(function (response) {
                        console.log(response);
                        alertService.showAlertMessage('Error', response, "error", true);
                    });
            if (!id) {
                openSelectionModal();
            }
            function openSelectionModal() {
                var modelItem = $mdDialog.show({
                    controller: 'ItemController',
                    templateUrl: 'client/app/itemSelectionModal.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    resolve: {
                        itemInfo: function () {
                            restItem["restaurantId"] = $scope.restaurantId;
                            return restItem;
                        }
                    }
                });
            }
        };

        $scope.isReadonly = true;
        $scope.max = 5;
        $scope.rate = 1;
        restaurantFactory.getRestaurantDetail($scope.restaurantId)
        .success(function (data) {
            restaurantDataService.restDetail = data;
            $scope.restaurantDetail = data.RestaurantDetail;
            $scope.menuItems = data.MenuItems;
        }).error(function (response) {
            console.log(response);
        });
    }]);

angular.module('app').controller('ItemController', [
'$scope', '$state', 'restaurantFactory', 'cartService', 'itemInfo', '$mdDialog', '$timeout', '$rootScope', 'localStorageService', 'alertService',
function ($scope, $state, restaurantFactory, cartService, itemInfo, $mdDialog, $timeout, $rootScope, localStorageService, alertService) {
    var cartId = cartService.getId();

    function addItem(item, cartId, resId, itemTotal) {
        cartService.addItem(item, cartId)
                   .success(function (result) {
                       if (result) {
                           item["restaurantId"] = resId;
                           cartService.saveCart(cartId);
                           localStorageService.set('restaurantId', resId);
                           $rootScope.$broadcast('itemAdded');
                           $rootScope.$broadcast('cartTotalItems', itemTotal + 1);
                           $scope.isAdded = true;
                           $timeout(function () {
                               $scope.showLoader = false;
                               $mdDialog.cancel();
                           },
                               1000);

                           // restaurantFactory.showAlertMessage("Save", "Cart item added sucessfully.", "success");
                       }
                   })
                   .error(function (response) {
                       console.log(response);
                   });
    };

    var errorCallBack = function (response) {
        if (response.status === 400) {
            cartService.deleteId();
            $mdDialog.hide();
        }

        if (response.status === 422) {

            // show this error cart. things it may show restaurant closed, 
        }
        console.log(response);
    };

    function selectedItems(selectedItem) {
        var itemCount = 0;
        var deliveryInfo = restaurantFactory.getSearchLocation();
        if (!deliveryInfo) {
            $mdDialog.hide();
            $state.go('app.home', {}, { reload: true });
            return;
        }
        if (!cartId) {
            cartService.clearCurrentCount();
            $rootScope.$broadcast('cartTotalItems', 0);
            var cartObj = {
                deliveryType: 0, deliveryWindow: new Date(), deliveryLat: deliveryInfo.lat, deliveryLon: deliveryInfo.lon
            }
            cartService.createCart(cartObj).then(function (response) {
                if (response.data)
                    cartId = response.data.Id;
                return cartId;
            }).then(function (cartId) {
                localStorageService.set("deliveryAddress", cartService.deliveryAddress);
                addItem(selectedItem, cartId, itemInfo.restaurantId, itemCount);
            }).catch(errorCallBack);
        }
        else {
            //cart id present in storage
            cartService.getItems(cartId)
                .then(function (response) {
                    if (response.data) {
                        itemCount = response.data.CartItems.length;
                        return itemCount;

                    }
                    return 0;
                }).then(function (count) {
                    if (count === 0) {
                        localStorageService.set("deliveryAddress", cartService.deliveryAddress);
                        var deliveryInfo = restaurantFactory.getSearchLocation();
                        if (deliveryInfo)
                            cartService.addIncompleteDelivery(cartId, deliveryInfo).then(function (result) {
                                addItem(selectedItem, cartId, itemInfo.restaurantId, count);
                                return;
                            });
                    }
                    if (cartId) {
                        addItem(selectedItem, cartId, itemInfo.restaurantId, count);
                    }
                }).catch(errorCallBack);
        }
    }

    //$scope.item = itemInfo;
    $scope.quantity = 1;

    $scope.option = []
    var optionValue = { "categoryId": "", "menuId": [] }

    restaurantFactory.getMenuDetail(itemInfo.Id, parseInt(itemInfo.restaurantId))
        .success(function (response) {
            $scope.item = response;
            debugger;
            for (var i = 0; i < response.menu_option_categories.length; i++) {
                $scope.option.push(angular.copy(optionValue));
                for (var j = 0; j < response.menu_option_categories[i].menu_options.length; j++) {
                    $scope.option[i].menuId.push(false);
                    if (response.menu_option_categories[i].minOption == 1 && response.menu_option_categories[i].maxOption == 1) break;
                }
            }
            $scope.itemPrice = response.item_price;
        }).error(function (response) {
            console.log(response);
        });

    $scope.findFreeMenu = function (menuOption) {
        if (menuOption.option_price <= 0) {
            if (menuOption.option_addOnPrice)
                return "+ $" + menuOption.option_addOnPrice;
            else
                return "FREE";
        }
    }

    $scope.optionSelected = function (category, menu, status) {
        $timeout(function () {
            for (var i = 0; i < $scope.option.length; i++) {
                for (var j = 0; j < $scope.option[i].menuId.length; j++) {
                    var catId = $scope.option[i].categoryId;
                    var menuId = $scope.option[i].menuId[j];
                    if (menuId) {
                        if (angular.isString(menuId)) {
                            if (menuId.split(',')[1] != j)
                                j = menuId.split(',')[1];
                        }
                        if ($scope.item.menu_option_categories[i].menu_options[j].option_price > 0)
                            $scope.item.item_price = $scope.item.menu_option_categories[i].menu_options[j].option_price;
                        if ($scope.item.menu_option_categories[i].menu_options[j].option_addOnPrice) {
                            $scope.item.item_price = $scope.item.item_price + $scope.item.menu_option_categories[i].menu_options[j].option_addOnPrice;
                        }
                    }
                }
            }
            $scope.itemPrice = $scope.quantity * $scope.item.item_price;
        }, 400)
    }

    $scope.changeQuantity = function (id, qty) {
        if (angular.isUndefined(qty) || qty > 99) {
            $scope.itemPrice = $scope.item.item_price;
            return;
        }
        $scope.itemPrice = qty * $scope.item.item_price;
        $scope.quantity = qty;
    }
    $scope.showLoader = false;

    $scope.addToBag = function () {
        $scope.showLoader = true;
        if (cartId) {
            cartService.getItems(cartId)
             .success(function (data) {
                 if (data) {
                     var isNew = true;
                     for (var index = 0; index < data.CartItems.length; index++) {
                         if ($scope.item.Id === data.CartItems[index].MenuId) {
                             isNew = false;
                             var updateItem = {
                                 "Id": data.CartItems[index].Id,
                                 "MenuId": data.CartItems[index].MenuId,
                                 "Quantity": data.CartItems[index].Quantity + $scope.quantity,
                                 "Notes": data.CartItems[index].Notes == null ? "" : data.CartItems[index].Notes,
                                 "RestaurantId": $scope.item.restaurantId
                             }
                             cartService.updateItem(updateItem, cartId);
                         }
                     }
                     if (isNew) {
                         addItemToCart();
                     }
                     $scope.isAdded = true;
                     $timeout(function () {
                         $scope.showLoader = false;
                         $mdDialog.cancel();
                     },
                         1500);
                 }
             })
            .error(errorCallBack);
        }
        else {
            addItemToCart();
            $scope.showLoader = false;
        }
    };

    function addItemToCart() {
        $scope.cartItem = {
            menuId: $scope.item.id,
            description: $scope.item.name,
            itemPrice: $scope.itemPrice,
            quantity: $scope.quantity,
            restaurantId: $scope.item.restaurantId
        }
        selectedItems($scope.cartItem);
    }

    $scope.cancel = function () {
        $mdDialog.cancel();
    };
}]);


