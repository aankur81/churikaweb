﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('OrderViewController', OrderViewController);

    OrderViewController.$inject = ['$location', '$scope', 'cartService', '$stateParams'];

    function OrderViewController($location, $scope, cartService, $stateParams) {
        var cartId = $stateParams.cartId;
        cartService.getItems(cartId).success(function (response) {
            $scope.cartDetails = response;
        }).error(function (response) {
            console.log("error :", response);
        })
    }
})();
