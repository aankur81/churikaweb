﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AlertController', AlertController);

    AlertController.$inject = ['$scope', '$mdDialog', 'cartService', 'message','$rootScope'];

    function AlertController($scope, $mdDialog, cartService, message,$rootScope) {
        $scope.message = message.content;
        $scope.title = message.title;
        $scope.type = message.type;
        $scope.showClearCart = message.showClearCart;
        $scope.cancel = function () {
            $mdDialog.cancel();
        }
        $scope.clearItems = function () {
            $rootScope.$broadcast("cartTotalItems", 0);
            cartService.deleteId();
            $mdDialog.cancel();
        }
    }
})();
