﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('FooterController', FooterController);

    FooterController.$inject = ['$scope', 'cartService', '$mdDialog', '$rootScope', ];

    function FooterController($scope, cartService, $mdDialog, $rootScope) {
        $scope.title = 'FooterController';

        activate();

        function activate() {
            $rootScope.showTabDialog = function (ev) {
                $mdDialog.show({
                    controller: 'CartController',
                    templateUrl: 'Client/app/cart.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            }
            $scope.$on("cartTotalItems",
                function (event, args) {
                    $scope.cartCount = args;
                });

            var cartId = cartService.getId();
            if (cartId) {
                cartService.getItems(cartId)
                 .success(function (data) {
                     if (data) {
                         $scope.cartCount = data.CartItems.length;
                     }
                 }).error(function () {
                     cartService.deleteId();
                     $mdDialog.hide();
                 });
            }
        }
    }
})();
