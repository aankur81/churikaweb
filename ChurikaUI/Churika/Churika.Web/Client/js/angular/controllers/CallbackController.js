﻿'use strict';
angular.module('app').controller('CallbackController', ['$scope', '$location', 'oidcManager', '$stateParams', '$window', 'localStorageService', '$state',
function ($scope, $location, oidcManager, $stateParams, $window, localStorageService, $state) {
    var hash = $stateParams.response;
    var mgr = oidcManager.OidcTokenManager;
    if (hash.charAt(0) === "&") {
        hash = hash.substr(1);
    }
    mgr.processTokenCallbackAsync(hash).then(function () {
        var redirect = localStorageService.get('url');
        if (redirect !== null)
            $state.go(redirect.url, redirect.param, { reload: true });
        else {
            $state.go('app.home', {}, { reload: true });
        }
        //window.location = localStorageService.get('url'); //window.location.protocol + "//" + window.location.host + "/";

    }, function (error) {
        console.log(error.message || error);
        //idmErrorService.error(error && error.message || error);
    });
}]);