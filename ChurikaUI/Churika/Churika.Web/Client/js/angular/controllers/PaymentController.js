﻿'use strict';
angular.module('app').controller('PaymentController', ['$scope', '$rootScope','cartService', '$state', 'localStorageService', '$stateParams', 'orderFactory', 'oidcManager', '$location', '$http', 'restaurantFactory', 'alertService',
    function ($scope, $rootScope,cartService, $state, localStorageService, $stateParams, orderFactory, oidcManager, $location, $http, restaurantFactory, alertService) {


        var mgr = oidcManager.OidcTokenManager;
        $scope.paid = false;
        var cartId = $stateParams.cartId;
        var resId = localStorageService.get('restaurantId');

        $scope.handleStripe = function (status, response) {
            if (response.error) {
                $scope.paid = false;
                $scope.message = "Error from Stripe.com";
            } else {
                var payInfo = {
                    'token': response.id,
                    'customer_id': 0,// $scope.reservation_info.customer_id,
                    'total': 1//$scope.reservation_info.total_price
                };
                if (!cartId) {
                    $state.go('app.home');
                }   
                else {
                    var orderComplete = {
                        "AddressId": 16,
                        "CartId": cartId,
                        "DeliveryType": 0,
                        "RestaurantId": parseInt(resId),
                        "UserId": 0,
                        "PaymentToken": payInfo.token,
                        "DeliveryTime": "2016-08-21t05:01:52.861z",
                        "DeliveryTip": 0
                    };
                    orderFactory.orderComplete(orderComplete)
                    .success(function (response) {
                        console.log("ordercomplete : ", response);
                        if (response) {
                            $rootScope.$broadcast('cartTotalItems', 0);
                            localStorageService.set("deliveryAddress", '');
                            cartService.deleteId();
                            localStorageService.set('restaurantId', 0);
                            $state.go('app.order-confirmation', { "id": response })
                        }
                    }).error(function (response) {
                        alertService.showAlertMessage("Error", response, "error");
                        console.log("ordercompleteerror : ", response);
                    });
                }
                //debugger;
                //console.log(payInfo);
                //$http.post('/api/payreservation', payInfo).success(function (data) {
                //    if (data.status == "OK") {
                //        $scope.paid = true;
                //        $scope.message = data.message;
                //    } else {
                //        $scope.paid = false;
                //        $scope.message = data.message;
                //    }
                //});
            }
        };

        //function getOrderInformation() {
        //    orderFactory.getOrder(1)
        //    .success(function (data) {
        //        console.log(data);
        //    }).error(function (response) {
        //        console.log(response);
        //    });
        //}    
        //if (mgr.expired) {
        //    localStorage["appState"] = $location.url();
        //    mgr.redirectForToken();
        //}
        //else {
        //    getOrderInformation();
        //}

    }]);