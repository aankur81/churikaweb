﻿'use strict';
angular.module('app').controller('ForgotPasswordController', ['$scope', '$stateParams', '$location', 'userFactory', 'oidcManager', function ($scope, $stateParams, $location, userFactory, oidcManager) {

    $scope.user = {};
    $scope.errors = [];
    $scope.isUpdated = false;
    $scope.dataLoading = false;
    $scope.isSubmitted = false;

    $scope.submitEmail = function () {
        userFactory.forgotPassword($scope.userEmail)
            .success(function (data) {
                $scope.isSubmitted = true;
                $scope.message = "An Email has been sent with Reset Password Request!";

            })
            .error(function (response) {
                $scope.isSubmitted = false;
                parseErrors(response);
            });
    }




    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);