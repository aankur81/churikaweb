﻿'use strict';
angular.module('app').controller('ChangePasswordController', ['$scope', '$stateParams', '$location', 'userFactory', 'oidcManager', function ($scope, $stateParams, $location, userFactory, oidcManager) {

    $scope.user = {};
    $scope.errors = [];
    $scope.isUpdated = false;
    $scope.dataLoading = false;


    $scope.updatePwd = function () {
        $scope.dataLoading = true;
        userFactory.updatePassword($scope.user)
            .success(function (data) {
                $scope.response = "Password Update Successful";
                $scope.isUpdated = true;
                $scope.dataLoading = false;
            })
            .error(function (message) {
                $scope.errors = parseErrors(message);
                $scope.isUpdated = false;
                $scope.dataLoading = false;
            });
    }

    function parseErrors(response) {
        var errors = [];
        for (var key in response.ModelState) {
            for (var i = 0; i < response.ModelState[key].length; i++) {
                errors.push(response.ModelState[key][i]);
            }
        }
        return errors;
    }

}]);