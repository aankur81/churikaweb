﻿'use strict';
angular.module('app').controller('CartController', [
    '$scope', '$stateParams', '$location', 'userFactory', '$state', 'cartService', '$mdDialog', '$rootScope', 'localStorageService','alertService',
    function ($scope, $stateParams, $location, userFactory, $state, cartService, $mdDialog, $rootScope, localStorageService,alertService) {


        var cartId = cartService.getId();// $stateParams.cartId;
        $scope.deliveryAddress = localStorageService.get("deliveryAddress");
        getItems();

        function getItems() {
            if (!cartId)
                return;

            cartService.getItems(cartId)
             .success(function (data) {
                 if (data) {
                     $scope.Id = data.Id;
                     $scope.cartItems = data.CartItems;                     
                     $rootScope.$broadcast('cartTotalItems', data.CartItems.length);
                     $scope.charge = data.Charge;                     
                     $scope.deliveryAddress = localStorageService.get("deliveryAddress");                     
                     console.log($scope.cartItems);
                 }
             })
               .error(function (data, status, headers, config) {
                    if (status === 400)
                        cartService.deleteId();

                   //pankaj: this is where you would need to check for status code from http and 
                   // status - 422: then show the error message to user in place of cart items
                   //status - 500: clear cart and show cart empty message
                   //status - 400: clear cart and show cart empty message,
                   // if search location from local storage is missing, clear cart and show empty cart message
                   console.log(status);
               });
        }

        $scope.editItem = function (ev, cartItem) {
            var modelItem = $mdDialog.show({
                controller: 'CartUpdateController',
                templateUrl: 'client/app/cartUpdate.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                resolve: {
                    itemInfo: function () {
                        cartItem["restaurantId"] = $stateParams.id;;
                        return cartItem;
                    }
                }
            });
        }

        $scope.removeItem = function (cartItem) {

            cartService.removeCartItem(cartId, cartItem.Id)
             .success(function (data) {
                 if (data) {
                     if ($scope.cartItems.length == 1) {
                         cartService.deleteId();
                         $scope.deliveryAddress = localStorageService.set("deliveryAddress", '');
                     }
                     cartService.removeItem(cartItem);
                     getItems();
                 }
             })
             .error(function (msg) {
                 console.log(msg);
             });
        }

        $scope.getTotalPrice = function () {
            var cartTotal = 0;
            if ($scope.cartItems) {
                for (var index = 0; index < $scope.cartItems.length; index++) {
                    cartTotal += $scope.cartItems[index].Price * $scope.cartItems[index].Quantity;
                }
            }
            return cartTotal;
        };
        $scope.getTotalCount = function () {
            var cartItemTotal = 0;
            if ($scope.cartItems) {
                for (var index = 0; index < $scope.cartItems.length; index++) {
                    cartItemTotal += $scope.cartItems[index].Quantity;
                }
            }
            return cartItemTotal;

        }
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.clearItems = function () {
            cartService.deleteId();
            cartService.clearCurrentCount();
            $scope.deliveryAddress = localStorageService.set("deliveryAddress", '');
            $rootScope.$broadcast('cartTotalItems', 0);
            $scope.cartItems = [];
            cartService.Items = [];
            //if ($scope.cartItems) {            
            //for (var index = 0; index < $scope.cartItems.length; index++) {
            //    cartService.removeCartItem(cartId, $scope.cartItems[index].Id)
            //     .success(function (data) {
            //         if (data) {
            //             //   cartService.removeItem(cartItem);                    
            //             getItems();
            //         }
            //     })
            //     .error(function (msg) {
            //         console.log(msg);
            //     });
            //}
            //}
        }
        $scope.checkout = function (cartId) {
            $mdDialog.cancel();
            cartService.saveCheckoutUrl($state.current.name, $stateParams);
            $state.go('app.checkout.delivery', { cartId: cartId });
        }
        $scope.updateDeliveryAddress = function () {

        };
    }]);