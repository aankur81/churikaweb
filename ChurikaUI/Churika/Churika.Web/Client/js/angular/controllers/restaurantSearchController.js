﻿'use strict';

angular.module('app').controller('RestaurantSearchController', ['$scope', '$location', 'restaurantFactory', '$state', 'restaurantDataService', '$rootScope', 'cartService', 'cfpLoadingBar', '$mdDialog',
function ($scope, $location, restaurantFactory, $state, restaurantDataService, $rootScope, cartService, cfpLoadingBar, $mdDialog) {
    $scope.showScheduleModal = function () {
        var modelItem = $mdDialog.show({
            controller: 'ScheduleController',
            templateUrl: 'client/app/dialog/schedule.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
    }

    if ($state.current.name === 'app.restaurants.list' || $state.current.name === 'app.restaurants.detail')
        $scope.isFilterPage = true;
    else
        $scope.isFilterPage = false;

    var searchInfo = restaurantFactory.getSearchParam();
    $scope.place = searchInfo ? searchInfo.place : {};
    $scope.error = '';

    if ($state.params.query)
        $scope.searchTerm = $state.params.query;
    else
        $scope.searchTerm = searchInfo ? searchInfo.query : '';

    $scope.latitude = searchInfo ? searchInfo.lat : 0.0;
    $scope.longitude = searchInfo ? searchInfo.lon : 0.0;

    //saveQueryParams();

    var geocoder = new google.maps.Geocoder;
    $scope.autocompleteOptions = {
        componentRestrictions: { country: 'us' },
        types: ['geocode']
    }

    function getRestaurants() {

        searchInfo = restaurantFactory.getSearchParam();
        if (searchInfo == null || searchInfo.lat === 0.0 || searchInfo.lon === 0.0) {
            restaurantFactory.clearSearchParam();
            $state.go('app.home', {}, { reload: true });
            return;
        }
        $state.go('app.restaurants.list', { query: searchInfo.query }, { reload: false });
    }

    function saveQueryParams() {
        var searchParams = { lat: $scope.latitude, lon: $scope.longitude, query: $scope.searchTerm, page: 1, place: $scope.place };
        restaurantFactory.saveSearchParams(searchParams);
    }

    $scope.searchRest = function (foodCategory) {
        saveParametersAndGetRestaurants();
    };

    function saveParametersAndGetRestaurants() {
        saveQueryParams();
        getRestaurants();
    }

    $scope.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                $scope.error = "You denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                $scope.error = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                $scope.error = "The request to get user location timed out.";
                break;
            case error.UNKNOWN_ERROR:
                $scope.error = "An unknown error occurred.";
                break;
        }
        $scope.$apply();
    }

    function getCurrentPositionCoords(latitude, longitude) {
        $scope.latitude = parseFloat(latitude);
        $scope.longitude = parseFloat(longitude);

        var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            var address = null;
            $scope.$apply(function () {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length > 0) {
                        $scope.place = results[0].formatted_address;
                        address = {
                            "Address1": results[0].address_components[0].short_name,
                            "address2": "",
                            "City": results[0].address_components[1].short_name,
                            "StateCode": results[0].address_components[3].short_name,
                            "County": results[0].address_components[4].short_name,
                            "PostalCode": results[0].address_components[5].short_name,
                        }

                    } else {
                        console.log("No results found");
                    }
                }
                else {
                    alert("Geocoder failed due to: " + status);
                }
            });
            if (address) {
                cartService.deliveryAddress = address;//$scope.place;
                //$rootScope.$broadcast('deliveryAddress', { address: address });
            }
        });
    };

    $scope.getLocation = function () {
        cfpLoadingBar.start();
        window.navigator.geolocation.getCurrentPosition(function (position) {
            getCurrentPositionCoords(position.coords.latitude, position.coords.longitude);
            var locationInfo = { lat: position.coords.latitude, lon: position.coords.longitude, isIncomplete: true }
            restaurantFactory.saveSearchLocation(locationInfo); //save missing delivery information when user requests to detect location
            cfpLoadingBar.complete();
        }, function (error) {
            cfpLoadingBar.complete();
            $scope.showError(error);
        });
    }

    $scope.close = function () {
        $scope.error = '';
    }

    $scope.accessUserLocation = function () {
        $scope.getLocation();
    }

    $scope.$on('g-places-autocomplete:select', function (event) {

        var locationTypes = event.targetScope.model.types;
        var isIncomplete = false;

        if (locationTypes.indexOf('street_address') === -1) {
            isIncomplete = true;
        }

        $scope.latitude = event.targetScope.model.geometry.location.lat();
        $scope.longitude = event.targetScope.model.geometry.location.lng();

        var locationInfo = { lat: $scope.latitude, lon: $scope.longitude, isIncomplete: isIncomplete }
        restaurantFactory.saveSearchLocation(locationInfo);

        console.log($scope.latitude);
    });


    //if ($scope.latitude && $scope.longitude)
    //    getCurrentPositionCoords($scope.latitude, $scope.longitude);
    //console.log($scope.place);
}]);

angular.module('app').controller('ScheduleController', ['$scope', '$mdDialog',
function ($scope, $mdDialog) {
    $scope.cancel = function () {
        $mdDialog.cancel();
    }
}
]);