﻿angular.module('app')
    .factory('userFactory', ['$http', 'oidcManager', 'settings', function ($http, oidcManager, settings) {

        var urlBase = settings.apiUrl + "/api/account";
        var dataFactory = {};
        var mgr = oidcManager.OidcTokenManager;


        dataFactory.submitRegistration = function (data) {
            console.log(urlBase + "/Register");

            return $http.post(urlBase + '/Register', data, {
                ignoreLoadingBar: false
            });
        };

        dataFactory.updatePassword = function (data) {
            return $http.post(urlBase + '/ChangePassword', data, {
                ignoreLoadingBar: false,
                headers: { 'Authorization': 'Bearer  ' + mgr.access_token }
            });
        };

        dataFactory.forgotPassword = function (email) {
            console.log(email);
            return $http.post(urlBase + '/ForgotPassword', '"' + email + '"', {
                ignoreLoadingBar: false
            });
        };

        dataFactory.resetPassword = function (data) {
            return $http.post(urlBase + '/ResetPassword', data, {
                ignoreLoadingBar: false
            });
        };

        dataFactory.updateProfile = function (data) {
            return $http.post(urlBase + '/UpdateProfile', data, {
                ignoreLoadingBar: false,
                headers: { 'Authorization': 'Bearer  ' + mgr.access_token }
            });
        };

        dataFactory.getProfile = function () {
            return $http.get(urlBase + '/UserProfile', {
                ignoreLoadingBar: false,
                headers: { 'Authorization': 'Bearer  ' + mgr.access_token }
            });
        };
        return dataFactory;

    }]);