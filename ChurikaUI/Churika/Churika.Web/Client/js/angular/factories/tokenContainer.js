﻿'use strict';
angular.module('app').factory('tokenContainer', ['$http', '$location', 'localStorageService', function ($http, $location, localStorageService) {

    var container = {
        token: ""
    };

    var setToken = function (token) {
        container.token = token;
    };

    var getToken = function () {
        debugger;
        if (container.token === '') {
            if (localStorage.getItem("access_token") === null) {

                var authorizationUrl = 'https://localhost:44300/identity/connect/authorize';
                var client_id = 'ChurikaAngularApp';
                var redirect_uri = 'https://localhost:44302/client/app/callback.html';
                var response_type = "token";
                var scope = "churikaApiOrder";
                var url =
                    authorizationUrl + "?" +
                    "client_id=" + encodeURI(client_id) + "&" +
                    "redirect_uri=" + encodeURI(redirect_uri) + "&" +
                    "response_type=" + encodeURI(response_type) + "&" +
                    "scope=" + encodeURI(scope) + "&";

                window.location = url;

            } else {
                setToken(localStorage.getItem("access_token"));
            }
        }

        return container;
    }

    return {
        getToken: getToken
    };
}]);