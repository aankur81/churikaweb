﻿'use strict';
angular.module('app').factory('restaurantFactory', ['$http', '$location', 'settings', 'localStorageService',
    function ($http, $location, settings, localStorageService) {

        var urlBase = settings.apiUrl + '/api/Restaurant/';
        var service = {};

        service.restaurantsInfo = {};
        service.searchRestaurants = function (latitude, longitude, searchTerm, page) {
            return $http.get(urlBase + 'FindRestaurants', {
                ignoreLoadingBar: false,
                params: { latitude: latitude, longitude: longitude, page: page, searchTerm: searchTerm }
            });
        };

        service.getRestaurantDetail = function (restId) {
            return $http.get(urlBase + 'RestaurantDetail', {
                ignoreLoadingBar: false,
                params: { restaurantId: restId }
            });
        };
        service.getMenuDetail = function (menuId, restaurantId) {
            return $http.get(urlBase + restaurantId + '/menu/' + menuId, {
                ignoreLoadingBar: false
            })
        };
            
            
        service.saveSearchParams = function (searchParamsObj) {
            localStorageService.set('searchParam', searchParamsObj);
        };

        service.saveSearchLocation = function (locationInfo) {
            localStorageService.set('searchLocation', locationInfo);
        };

        service.getSearchLocation = function () {
            return localStorageService.get('searchLocation');
        }

        service.getSearchParam = function () {
            return localStorageService.get('searchParam');
        }

        service.clearSearchParam = function () {
            localStorageService.remove('searchParam');
        }

        return service;
    }]);