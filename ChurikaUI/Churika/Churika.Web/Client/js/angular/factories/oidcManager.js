﻿angular.module('app')
    .factory('oidcManager', ['appConfig', 'oidcConfig', function (appConfig, oidcConfig) {
        var mgr = new OidcTokenManager(oidcConfig.Config);
        return {
            OidcTokenManager: mgr
        };
    }]);