﻿angular.module('app')
    .factory('orderFactory', ['$http', 'oidcManager', 'settings', function ($http, oidcManager, settings) {

        var urlBase = settings.apiUrl + '/api/order';
        var dataFactory = {};
        var mgr = oidcManager.OidcTokenManager;

        dataFactory.getOrder = function (id) {
            return $http.get(urlBase + '/OrderDetail', {
                headers: {
                    'Authorization': 'Bearer  ' + mgr.access_token,
                    "isAuthenticate": 'true'
                },
                ignoreLoadingBar: false,
                params: { id: id }
            });
        };

        dataFactory.orderComplete = function (order) {
            return $http.post(urlBase + '/Complete', order, {
                ignoreLoadingBar: true
            });
        };

        return dataFactory;
    }]);