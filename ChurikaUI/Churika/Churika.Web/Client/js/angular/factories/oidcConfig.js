﻿angular.module('app')
    .factory('oidcConfig', ['appConfig', '$window', 'settings', function (appConfig, $window, settings) {
        var config = {
            client_id: settings.identityClientId,
            redirect_uri: settings.clientCallbackUrl,
            response_type: "id_token token",
            scope: "openid profile all_claims",
            authority: settings.identityUrl,
            post_logout_redirect_uri: $window.location.protocol + "//" + $window.location.host + "/",
            silent_redirect_uri: settings.silentCallbackUrl,
            silent_renew: true,
            store: window.sessionStorage
        };
        // console.log(window.oidcConfiguration);

        return {
            Config: config
        };
    }]);