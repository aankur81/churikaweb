﻿'use strict';



angular.module('app')
    .factory('appConfig', ['$window', function ($window) {
        var host = $window.location.hostname;
        var config = {};
        //console.log(host);

        config = {
            apiUrl: "https://churikapi.azurewebsites.net",
            identityUrl: "https://churikaaccount.azurewebsites.net/identity",
            callbackUrl: "https://churikaapp.azurewebsites.net/#/app/callback/",
            silentCallbackUrl: "https://churikaapp.azurewebsites.net/client/app/silentrefresh.html"
        }

        return {
            Config: config
        };
    }]);