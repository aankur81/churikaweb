﻿'use strict';
angular.module('app').factory('authInterceptor', ['localStorageService', '$injector', '$q', 'oidcManager', function (localStorageService, $injector, $q, oidcManager) {
    var authInterceptorFactory = {};
    var mgr = oidcManager.OidcTokenManager;


    var request = function (config) {
        //debugger;
        //if (config.headers.isAuthenticate) {
        //    debugger;
        //    if (mgr.expired) {
        //        mgr.redirectForToken();
        //    }

        //    config.headers.Authorization = 'Bearer  ' + mgr.access_token;
        //}
        config.headers = config.headers || {};
        config.headers.Authorization = 'Bearer  ' + mgr.access_token;

        return config;
    }

    var responseError = function (rejection) {
        switch (rejection.status) {
            case 401:
                mgr.redirectForToken();
                break;
            case 500:
                //TODO: show alert message from rejection.message
                break;
        }
        return $q.reject(rejection);
    }

    authInterceptorFactory.request = request;
    authInterceptorFactory.responseError = responseError;
    return authInterceptorFactory;
}]);