﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('cartNotifyService', cartNotifyService);

    cartNotifyService.$inject = ['$http', '$rootScope', 'appConfig', 'localStorageService'];

    function cartNotifyService($http, $rootScope) {

        var service = {

            subscribe: function (scope, callback) {
                var handler = $rootScope.$on('notifying-service-event', callback);
                scope.$on('$destroy', handler);
            },

            notify: function () {
                $rootScope.$emit('notifying-service-event');
            }
        };

        return service;
    }
})();