﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('cartService', cartService);

    cartService.$inject = ['$http', '$rootScope', 'settings', 'localStorageService'];

    function cartService($http, $rootScope, settings, localStorageService) {

        var urlBase = settings.apiUrl + "/api/cart";

        var service = {
            Items: [],

            addItem: function (item, cartId) {
                return $http.post(urlBase + '/' + cartId + '/add', item, {
                    ignoreLoadingBar: false
                });
            },

            updateItem: function (item, cartId) {
                return $http.put(urlBase + '/' + cartId + '/update', item, {
                    ignoreLoadingBar: false
                });
            },

            removeItem: function (item, caller) {
                for (var index = 0; index < this.Items.length; index++) {
                    if (this.Items[index].menuId == item.MenuId) {
                        this.Items.splice(index, 1);
                    }
                }
                localStorageService.set('cartTotalItems', this.Items.length);
                $rootScope.$broadcast('itemRemoved');
            },
            removeCartItem: function (cartId, cartItemId) {
                return $http.delete(urlBase + '/' + cartId + '/delete/' + cartItemId, {
                    ignoreLoadingBar: false
                });

            },
            saveCart: function (cartId) {
                var cartObj = { cartId: cartId }
                localStorageService.set('cartState', cartId);
            },

            createCart: function (cartInfo) {
                return $http.post(urlBase + '/Create', cartInfo, {
                    ignoreLoadingBar: false
                });
            },

            currentCount: function () {
                return localStorageService.get('cartTotalItems');//this.Items.length;
            },
            clearCurrentCount: function () {
                return localStorageService.set('cartTotalItems', 0);
            },


            getId: function () {
                var id = null;

                if (localStorageService != null) {
                    id = localStorageService.get('cartState');
                }
                return id;
            },

            deleteId: function () {
                localStorageService.set('cartState', '');
            },

            getItems: function (cartId) {                
                return $http.get(urlBase + '/' + cartId);
            },

            saveCheckoutUrl: function (url, paremeter) {
                localStorageService.set('url', { "url": url, "param": paremeter });
            },

            addIncompleteDelivery: function (cartId, deliveryInfo) {
                var data = {
                    "lat": deliveryInfo.lat,
                    "lon": deliveryInfo.lon
                }
                return $http.post(urlBase + '/' + cartId + '/delivery', data, {
                    ignoreLoadingBar: true
                });
            }

        };
        return service;
    }
})();