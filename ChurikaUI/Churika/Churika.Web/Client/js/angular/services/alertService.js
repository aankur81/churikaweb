﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('alertService', alertService);

    alertService.$inject = ['$http', '$mdDialog'];

    function alertService($http, $mdDialog) {
        var service = {};
        service.showAlertMessage = function (title, content, type, showClearCart) {
            var itemInfo = { 'title': title, 'content': content, 'type': type, 'showClearCart': showClearCart }
            //alert = $mdDialog.alert({
            //    title: title,
            //    textContent: content,
            //    ok: 'Close'
            //});
            //$mdDialog
            //  .show(alert)
            //  .finally(function () {
            //      alert = undefined;
            //  });
            $mdDialog.show({
                controller: 'AlertController',
                templateUrl: 'Client/app/dialog/alert.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    message: itemInfo
                }
            });
        };




        return service;
    }
})();