﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('deliveryService', deliveryService);

    deliveryService.$inject = ['$http', '$rootScope', 'settings', 'localStorageService'];

    function deliveryService($http, $rootScope, settings, localStorageService) {

        var urlBase = settings.apiUrl + "/api/address";

        var service = {
            postFilledAddress: function (cartId) {
                return localStorageService.set('isFilledAddress', { "status": true, "cartId": cartId });
            },

            getFilledAddress: function (cartId) {
                return localStorageService.get('isFilledAddress', { "status": true, "cartId": cartId });
            },

            addDeliveryAddress: function (address) {
                return $http.post(urlBase + '/Add', address, {
                    ignoreLoadingBar: true,
                    headers: {
                        "isAuthenticate": 'true'
                    }
                });
            },
            updateDeliveryAddress: function (address) {
                return $http.post(urlBase + '/Update', address, {
                    ignoreLoadingBar: true,
                    headers: {
                        "isAuthenticate": 'true'
                    }
                });
            },
            getDeliveryAddress: function (address) {
                return $http.get(urlBase + '/GetAll');
            }
        }
        return service;
    }
})();