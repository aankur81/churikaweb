﻿
angular.module('app', ['angular-loading-bar',
'ui.router',
'google.places',
'ngMessages',
'ui.bootstrap',
'LocalStorageModule',
'ngAnimate',
'ngAria',
'ngMaterial',
'ui.bootstrap.tpls',
'configurationProvider',
'angularPayments',
'angular-flexslider'
]);
// configuring our routes 
angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'settings', function ($stateProvider, $urlRouterProvider, $httpProvider, settings) {
    $stateProvider
        .state('app', {
            abstract: true,
            url: '/app',
            views: {
                'navigation': {
                    templateUrl: 'client/app/navigation.html',
                    controller: 'NavigationController'
                },
                'main': { template: '<div ui-view></div>' },
                'footer': {
                    templateUrl: 'client/app/footer.html',
                    controller: 'FooterController'
                },
            }
        })
        .state('app.home', {
            url: '/home',
            views: {
                '': {
                    templateUrl: 'client/app/home.html',
                    controller: 'HomeController'
                },

                'searchView@app.home': {
                    templateUrl: 'client/app/search.html',
                    controller: 'RestaurantSearchController'
                }
            }
        })
        .state('app.about', {
            url: '/about',
            templateUrl: 'client/app/about.html',
            controller: 'AboutController'
        })
        .state('app.restaurants', {
            abstract: true,
            url: '/restaurant',
            templateUrl: 'client/app/restaurants.html'
        })
        .state('app.restaurants.list', {
            url: '/list?query',
            views: {
                '': {
                    templateUrl: 'client/app/restaurantsList.html',
                    controller: 'RestaurantController'
                },

                'searchView@app.restaurants.list': {
                    templateUrl: 'client/app/search.html',
                    controller: 'RestaurantSearchController'
                }
            },
            resolve: {
                restaurantsListObj: [
                 "$state", "restaurantFactory", "$q", "$stateParams",
    function ($state, restaurantFactory, $q, $stateParams) {
        var deferred = $q.defer();
        var output = {};
        var searchInfo = restaurantFactory.getSearchParam();

        if (searchInfo == null || searchInfo.lat === 0.0 || searchInfo.lon === 0.0) {
            restaurantFactory.clearSearchParam();
            deferred.reject('No search params');
        }

        if ($stateParams.query !== searchInfo.query) {
            searchInfo.query = $stateParams.query;
            restaurantFactory.saveSearchParams(searchInfo);
        }

        output.searchParams = searchInfo;
        restaurantFactory
            .searchRestaurants(searchInfo.lat, searchInfo.lon, searchInfo.query, 1)
            .then(function (response) {
                output.result = response.data;
                deferred.resolve(output);
            });

        return deferred.promise;
    }
                ]
            }
        })
        .state('app.restaurants.detail', {
            url: '/:id',
            templateUrl: 'client/app/restaurantDetail.html',
            controller: 'RestaurantDetailController'
        })
        .state('app.restaurants.filter', {
            url: '/filter',
            templateUrl: 'client/app/food-detail.html',
            controller: 'HomeController'
        })
        .state('app.order', {
            url: '/order',
            templateUrl: 'client/app/order.html',
            controller: 'OrderController',
            authenticate: true
        })
        .state('app.order-confirmation', {
            url: '/order-confirmation/:id',
            templateUrl: 'client/app/order-confirmation.html',
            controller: 'OrderController',
            authenticate: true
        })
        .state('app.register', {
            url: '/Register',
            templateUrl: 'client/app/Registration.html',
            controller: 'RegistrationController'
        })
        .state('app.changePassword', {
            url: '/ChangePassword',
            templateUrl: 'client/app/changePassword.html',
            controller: 'ChangePasswordController',
            authenticate: true
        })
        .state('app.forgotPassword', {
            url: '/ForgotPassword',
            templateUrl: 'client/app/forgotPasswordRequest.html',
            controller: 'ForgotPasswordController',
            authenticate: false
        })
        .state('app.resetPassword', {
            url: '/resetPassword?code',
            templateUrl: 'client/app/resetPassword.html',
            controller: 'ResetPasswordController',
            authenticate: false
        })
        .state('app.authCallback', {
            url: '/callback/:response',
            templateUrl: 'client/app/callback.html',
            controller: 'CallbackController',
            authenticate: false
        })
        .state('app.cart', {
            url: '/:cartId/cart',
            templateUrl: 'client/app/cart.html',
            controller: 'CartController',
            authenticate: false
        })
        .state('app.checkout', {
            url: '/checkout',
            abstract: true,
            templateUrl: 'client/app/checkout.html',
            authenticate: true
        })
        .state('app.checkout.delivery', {
            url: '/:cartId/deliveryInfo',
            templateUrl: 'client/app/deliveryAddress.html',
            views: {
                '': {
                    templateUrl: 'client/app/deliveryAddress.html',
                    controller: 'AddressController'
                },
                'orderView@app.checkout.delivery': {
                    templateUrl: 'client/app/orderView.html',
                    controller: 'OrderViewController'
                }
            },
            authenticate: true
        })
        .state('app.checkout.pay', {
            url: '/:cartId/pay',
            templateUrl: 'client/app/pay.html',
            views: {
                '': {
                    templateUrl: 'client/app/pay.html',
                    controller: 'PaymentController'
                },
                'orderView@app.checkout.pay': {
                    templateUrl: 'client/app/orderView.html',
                    controller: 'OrderViewController'
                }
            },
            data: {
                redirect: ['deliveryService', 'cartService', function (deliveryService, cartService) {
                    var detail = deliveryService.getFilledAddress();
                    var cartId = cartService.getId();
                    if (!cartId)
                        return 'app.home';
                    else if (!detail) {
                        return 'app.checkout.delivery';
                    }
                    else if (!detail.status || cartId != detail.cartId) {
                        return 'app.checkout.delivery';
                    }
                }]
            },
            authenticate: true
        })
    .state('app.profile', {
        url: '/profile',
        templateurl: 'client/app/profile.html',
        controller: 'ProfileController',
        authenticate: true
    });
    // catch all route

    // send users to the form page 
    $urlRouterProvider.otherwise('app/home');
    window.Stripe.setPublishableKey(settings.stripeKey);
    $httpProvider.interceptors.push('authInterceptor');
}]);

angular.module('app').run(['$rootScope', '$state', 'oidcManager', '$window', 'appConfig', 'cfpLoadingBar', '$injector', function ($rootScope, $state, oidcManager, $window, appConfig, cfpLoadingBar, $injector) {

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        cfpLoadingBar.start();
        var manager = oidcManager.OidcTokenManager;
        if (toState.authenticate && manager.expired) {
            // User isn’t authenticated
            manager.redirectForToken();
            event.preventDefault();
        }
        if (toState.data && toState.data.redirect) {
            var redirectTo = $injector.invoke(toState.data.redirect, this, {
                toStateParams: toParams,
                toState: toState
            });
            console.log(redirectTo);

            if (redirectTo) {
                event.preventDefault();
                $state.go(redirectTo, toParams, { reload: true });
            }
        }
    });
    $rootScope.$on('$stateChangeSuccess', function () {
        $state.current.name.indexOf("app.checkout") != -1 || $state.current.name.indexOf("app.order-confirmation") != -1 ? $rootScope.hideCartIcon = true : $rootScope.hideCartIcon = false;
        cfpLoadingBar.complete();
    });

    $rootScope.$on('$stateChangeError',
    function (event, toState, toParams, fromState, fromParams, error) {
        $state.go('app.home', {}, { reload: true });
        cfpLoadingBar.complete();
    });
}]);


