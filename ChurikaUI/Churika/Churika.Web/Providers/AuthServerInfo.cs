﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Churika.Web.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;

    namespace Churika.Identity.Providers
    {
        public class AuthServerInfo
        {
            public static string PostLogoutRedirect
            {
                get { return ClientBaseUrl + "/"; }
            }

            public static string BaseAddress
            {
                get
                {
                    return ConfigurationManager.AppSettings["IdentityBaseUrl"];
                }
            }
            public static string IdentityPublicOrigin
            {
                get { return BaseAddress + "/"; }
            }

            public static string IdentityIssuerUri
            {
                get { return BaseAddress + "/identity"; }
            }

            public static string ClientRegistrationUrl
            {
                get { return ClientBaseUrl + "/#/app/Register"; }
            }

            private static string ClientBaseUrl
            {
                get { return ConfigurationManager.AppSettings["ClientBaseUrl"]; }
            }

            public static string ClientCallbackUrl
            {
                get { return ClientBaseUrl + "/#/app/callback/"; }
            }

            public static string ClientSilentCallbackUrl
            {
                get { return ClientBaseUrl + "/client/app/silentrefresh.html"; }
            }
        }
    }
}