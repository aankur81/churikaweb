namespace Churika.Web.Models
{
    public class SettingsDto
    {
        //public string WebApiVersion { get; set; }
        public string apiUrl { get; set; }
        public string IdentityUrl { get; set; }
        public string StripeKey { get; set; }
        public string IdentityClientId { get; internal set; }
        public string IdentityPublicOrigin { get; internal set; }
        public string ClientCallbackUrl { get; internal set; }
        public string ClientPostLogoutRedirectUri { get; internal set; }
        public string ClientSilentCallbackUrl { get; internal set; }
    }
}