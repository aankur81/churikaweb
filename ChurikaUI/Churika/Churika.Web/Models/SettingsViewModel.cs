namespace Churika.Web.Models
{
    public class SettingsViewModel  
    {
        public string SettingsJson { get; set; }
        public string AngularModuleName { get; set; }
    }
}