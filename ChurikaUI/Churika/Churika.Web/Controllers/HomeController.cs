﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Churika.Web.Models;
using Churika.Web.Providers.Churika.Identity.Providers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Churika.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.StripeKey = ConfigurationManager.AppSettings["StripePublishableKey"];
            return View();
        }

        [ChildActionOnly]
        public ActionResult Settings()
        {
            var settings = new SettingsDto
            {
                apiUrl = GetAppSetting<string>("ApiBaseUrl"),
                IdentityUrl = AuthServerInfo.IdentityIssuerUri,
                StripeKey = GetAppSetting<string>("StripeKey"),
                IdentityClientId = GetAppSetting<string>("IdentityClientId"),
                IdentityPublicOrigin = AuthServerInfo.IdentityPublicOrigin,
                ClientCallbackUrl = AuthServerInfo.ClientCallbackUrl,
                ClientPostLogoutRedirectUri = AuthServerInfo.PostLogoutRedirect,
                ClientSilentCallbackUrl = AuthServerInfo.ClientSilentCallbackUrl
            };

            var settingsJson = JavaScriptConvert.SerializeObject(settings);
            return PartialView(settingsJson);
        }

        protected static T GetAppSetting<T>(string key)
        {
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
        }
    }

    public static class JavaScriptConvert
    {
        public static IHtmlString SerializeObject(object value)
        {
            using (var stringWriter = new StringWriter())
            using (var jsonWriter = new JsonTextWriter(stringWriter))
            {
                var serializer = new JsonSerializer
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                jsonWriter.QuoteName = false;
                serializer.Serialize(jsonWriter, value);

                return new HtmlString(stringWriter.ToString());
            }
        }
    }
}
