﻿using System.Web.Optimization;

namespace Churika.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/loading-bar.min.css",
                      "~/Content/autocomplete.css",
                      "~/Client/js/angular/style/flexslider.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                                         "~/Client/js/angular/lib/angular.min.js",
                                         "~/Client/js/angular/lib/loadingbar.js",
                                         "~/Client/js/angular/lib/jquery.flexslider.js",
                                         "~/Client/js/angular/lib/angular-flexslider.js"
                                      ));

            bundles.Add(new ScriptBundle("~/bundles/angularModules").Include(
                                         "~/Client/js/angular/lib/autocomplete.js",

                                         //"~/Client/js/angular/lib/oidc-token-manager.js",

                                         "~/Client/js/angular/appModule.js",
                                         "~/Client/js/angular/config/appConfig.js",

                                          "~/Client/js/angular/factories/oidcConfig.js",
                                         //"~/Client/js/angular/factories/tokenContainer.js",
                                         "~/Client/js/angular/factories/restaurantFactory.js",
                                         "~/Client/js/angular/factories/userFactory.js",
                                         "~/Client/js/angular/factories/orderFactory.js",
                                         "~/Client/js/angular/factories/oidcManager.js",
                                         "~/Client/js/angular/services/authInterceptor.js",

                                         "~/Client/js/angular/services/restaurantDataService.js",
                                         "~/Client/js/angular/services/cartService.js",
                                         "~/Client/js/angular/services/deliveryService.js",
                                         "~/Client/js/angular/services/alertService.js",


                                         "~/Client/js/angular/controllers/NavigationController.js",
                                         "~/Client/js/angular/controllers/HomeController.js",
                                         "~/Client/js/angular/controllers/RestaurantSearchController.js",
                                         "~/Client/js/angular/controllers/RestaurantController.js",
                                         "~/Client/js/angular/controllers/RestaurantDetailController.js",
                                         "~/Client/js/angular/controllers/AboutController.js",
                                         "~/Client/js/angular/controllers/OrderController.js",
                                         "~/Client/js/angular/controllers/RegistrationController.js",
                                         "~/Client/js/angular/controllers/ChangePasswordController.js",
                                         "~/Client/js/angular/controllers/ForgotPasswordController.js",
                                         "~/Client/js/angular/controllers/ResetPasswordController.js",
                                         "~/Client/js/angular/controllers/CallbackController.js",
                                         "~/Client/js/angular/controllers/PaymentController.js",
                                         "~/Client/js/angular/controllers/AddressController.js",
                                          "~/Client/js/angular/controllers/CartController.js",
                                          "~/Client/js/angular/controllers/CartUpdateController.js",
                                          "~/Client/js/angular/controllers/ProfileController.js",
                                          "~/Client/js/angular/controllers/OrderViewController.js",
                                          "~/Client/js/angular/controllers/AlertController.js",
                                          "~/Client/js/angular/controllers/FooterController.js"

                          ));
            bundles.Add(new ScriptBundle("~/bundles/directives").Include(
                "~/Client/js/angular/directives/custom.js"
                ));
        }
    }
}
