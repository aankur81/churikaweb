﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Churika.Web.Startup))]

namespace Churika.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
